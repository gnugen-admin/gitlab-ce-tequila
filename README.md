# Gitlab Community Edition - GNUgen-flavored edition

This version of [gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce/tree/master) implement the [Tequila authentification system](https://tequila.epfl.ch/) and some UI modifications (e.g. login/homepage).
